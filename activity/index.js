/*
<!-- First part of the activity: 

Mimic the expected output. 
1. Use a variable to contain the First Name and then print it on the console with a string before it that says "First Name: ".  
2. Use a variable to contain the Last Name and then print it on the console with a string before it that says "Last Name: ".
3. Use a variable to contain the age. Assign a number. And then print on the console with a string before it that says ("Age: ")
4. Print hobbies using an array. Mimic the output. 
5. Print Work Address using an object. Mimic the output. 

Second part of the Activity: 
1. A code will be given (inside the boodle, on the notes and through codeshare: https://codeshare.io/vwqNvy)
2. Find all the errors in the activity. It should be displayed inside of the console.
 -->

*/ 
let firstName = "Bruce";
console.log("First Name: " + firstName);

let lastName = "Mirana";
console.log("Last Name: "+ lastName)

let age = 18;
console.log("Age: " + age)

let hobbies = ["eating", "sleeping", "playing", "meowing"]
console.log("Hobbies:")
console.log(hobbies)

workAddress = {
	number: "123",
	street: "Persian Street",
	city: "Catlandia",
	state: "District of Maincoone",
}
console.log("Work Address:")
console.log(workAddress)

// Activity 2


let fullName = "Steve Rogers";
console.log("My full name is " + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ["Tony","Bruce","Thor" ,"Natasha" ,"Clint", "Nick"];
console.log("My Friends are: ")
console.log(friends);

profile = {

	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false,

}
console.log("My Full Profile: ")
console.log(profile);

fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);

const lastLocation = "Arctic Ocean";
nextLocation = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);

